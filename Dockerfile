# This file is a template, and might need editing before it works on your project.
FROM python:3.6
EXPOSE 80
ENV HTTPS_PROXY "https://128.230.247.170:30000"
COPY ./app/hello-world.py ./
CMD ["python", "hello-world.py"]
